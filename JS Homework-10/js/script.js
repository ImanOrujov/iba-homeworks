let passwordSwitcher = false;
let eyeBtnPassword = document.querySelector(".icon-password");
let input = document.querySelector(".input");

let inputConfirmation = document.querySelector(".input-confirm");
eyeBtnPassword.addEventListener("click", () => {
    if (passwordSwitcher === false) {
        input.setAttribute("type", "text");
        passwordSwitcher = true;
        eyeBtnPassword.classList.remove("fa-eye-slash");
        eyeBtnPassword.classList.add("fa-eye")
    } else if (passwordSwitcher === true) {
        input.setAttribute("type", "password");
        passwordSwitcher = false;
        eyeBtnPassword.classList.remove("fa-eye");
        eyeBtnPassword.classList.add("fa-eye-slash")
    }
});
let eyeBtnConfirm = document.querySelector(".icon-password-confirm");
eyeBtnConfirm.addEventListener("click", () => {
    if (passwordSwitcher === false) {
        inputConfirmation.setAttribute("type", "text");
        passwordSwitcher = true;
        eyeBtnConfirm.classList.remove("fa-eye-slash");
        eyeBtnConfirm.classList.add("fa-eye")
    } else if (passwordSwitcher === true) {
        inputConfirmation.setAttribute("type", "password");
        passwordSwitcher = false;
        eyeBtnConfirm.classList.remove("fa-eye");
        eyeBtnConfirm.classList.add("fa-eye-slash")
    }
});


let submitBtn = document.querySelector(".btn");
submitBtn.addEventListener("click",()=>{
    if (input.value === inputConfirmation.value){
        alert("You are welcome");
    } else {
        alert("You need to enter the identical values");
    }
});