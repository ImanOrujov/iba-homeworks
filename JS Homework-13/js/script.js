localStorage.setItem('backgroundColor', 'black');
localStorage.setItem('color', 'white');
let theme = "white";

let button = document.addEventListener('click', changeColor);

function changeColor() {
    if (theme === "white") {
        document.body.style.backgroundColor = localStorage.getItem('backgroundColor');
        document.querySelector('.logo').style.color = localStorage.getItem('color');
        document.querySelector('.main-navbar').style.borderColor = localStorage.getItem('color');
        document.querySelector('.main-content-photo').style.borderColor = localStorage.getItem('color');
        let navbar = document.querySelectorAll('.main-navbar-href');
        navbar.forEach((item) => {
            item.style.color = localStorage.getItem('color');
            theme = "black";
        });
    }else if(theme==="black"){
        document.body.style.backgroundColor = localStorage.getItem('color');
        document.querySelector('.logo').style.color = localStorage.getItem('backgroundColor');
        document.querySelector('.main-navbar').style.borderColor = localStorage.getItem('backgroundColor');
        document.querySelector('.main-content-photo').style.borderColor = localStorage.getItem('backgroundColor');
        let navbar = document.querySelectorAll('.main-navbar-href');
        navbar.forEach((item) => {
            item.style.color = localStorage.getItem('backgroundColor');
            theme = "white"});
    }
}