$(".latest-news").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".header").offset().top
    }, 500);
});
$(".most-popular-news").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".posts").offset().top
    }, 1000);
});
$(".our-most-popular-clients").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".clients").offset().top
    }, 1500);
});
$(".top-rated").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".top").offset().top
    }, 2000);
});
$(".hot-news").click(function() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(".news").offset().top
    }, 2500);
});


jQuery(document).ready(function() {

    var btn = $('.top-button');

    $(window).scroll(function() {
        if ($(window).scrollTop() > window.innerHeight) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
    });

    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '500');
    });

});

$('.show-button').click(() => {
    $('.top').toggle('slow')
});