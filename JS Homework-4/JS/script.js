function createUser(name, surname) {
    let newUser = {
        firstName: name,
        lastName: surname,
        getLogin : function(){
            return console.log(this.firstName.substring(0,1).toLocaleLowerCase()+this.lastName.toLocaleLowerCase());
        }
    };
    return newUser;

}

let me = createUser("Iman", "Orujov");
me.getLogin();
console.log(me);

