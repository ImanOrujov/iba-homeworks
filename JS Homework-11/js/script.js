document.addEventListener('keyup',(event)=>{
   let enter = document.querySelector(".enter");
   let keyS = document.querySelector(".keyS");
   let keyE = document.querySelector(".keyE");
   let keyO = document.querySelector(".keyO");
   let keyN = document.querySelector(".keyN");
   let keyL = document.querySelector(".keyL");
   let keyZ = document.querySelector(".keyZ");

   if(event.key=="Enter"){
       enter.style.backgroundColor="blue";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="black";
   }else if(event.key=="s"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="blue";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="black";

   }else if(event.key=="e"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="blue";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="black";

   }else if(event.key=="o"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="blue";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="black";

   }else if(event.key=="n"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="blue";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="black";

   }else if(event.key=="l"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="blue";
       keyZ.style.backgroundColor="black";

   }else if(event.key=="z"){
       enter.style.backgroundColor="black";
       keyS.style.backgroundColor="black";
       keyE.style.backgroundColor="black";
       keyO.style.backgroundColor="black";
       keyN.style.backgroundColor="black";
       keyL.style.backgroundColor="black";
       keyZ.style.backgroundColor="blue";

   }
});