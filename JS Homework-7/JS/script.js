function displayArray(list, sublist) {
    const script = document.querySelector("script");
    let fragment = document.createDocumentFragment();
    let unorderedList = document.createElement("ul");
    unorderedList.style.fontSize="30px";

    list.map(value => {
        const item = document.createElement("li");
        item.textContent=value;
        unorderedList.appendChild(item);
        return unorderedList;

    });
    fragment.appendChild(unorderedList);
    script.before(fragment);
    setTimeout(()=>document.querySelector("ul").style.visibility="hidden", 10000);


    let fragmentTwo = document.createDocumentFragment();
    let unorderedSublist = document.createElement("ul");

    sublist.map(value => {
        const itemTwo = document.createElement("li");
        itemTwo.textContent=value;
        unorderedSublist.appendChild(itemTwo);
        return unorderedSublist;

    });
    fragmentTwo.appendChild(unorderedSublist);
    script.before(fragmentTwo);
}
displayArray(['hello', 'world', 'Baku', 'IBA Tech Academy', 2019],['1', '2', '3', 'sea', 'user', 23]);