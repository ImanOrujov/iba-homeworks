let photo = document.querySelector('.image-to-show');
photo.style.display="block";
let i = 1;
let stopButton = document.querySelector('.stop-button');
let startButton = document.querySelector('.start-button');
startButton.style.display='none';

function changePictures() {

    if (i < 4) {
        photo.src = `img/${++i}.jpg`;
    } else {
        i = 1;
        photo.src = `img/${i}.jpg`;
    }

}

changePictures();

let loop = setInterval(changePictures,1000);

stopButton.addEventListener('click',stopChanging);

function stopChanging() {
    stopButton.style.display='none';
    startButton.style.display='block';
    clearInterval(loop);
}

startButton.addEventListener('click',startChanging);

function startChanging() {
    startButton.style.display='none';
    stopButton.style.display='block';
    loop = setInterval(changePictures,1000);
}