let tabs = document.querySelector('.tabs');
let tabsTitle = document.querySelectorAll('.tabs-title');
let tabsContent = document.querySelectorAll('.li');
tabsTitle[0].classList.add('active');
tabsContent[0].style.display='flex';

function hideAllTabs(){
  for(let i = 0;i<tabsTitle.length;i++){
      tabsTitle[i].classList.remove('active');
      tabsContent[i].style.display='none';
  }
}

tabs.addEventListener('click',evt => {
   hideAllTabs();
   evt.target.classList.add('active');
   for (let i = 0; i<tabsContent.length; i++){
       if(evt.target.dataset.title === tabsContent[i].dataset.content){
           tabsContent[i].style.display='flex';
       }
   }
});
