let tabsTitle = $('.tabs-title');
let tabsContent = $('.li');
tabsTitle[0].classList.add('active');
tabsContent[0].style.display='flex';

$.fn.hideAllTabs = function () {
    for(let i = 0; i<this.length; i++){
        this[i].classList.remove('active');
        tabsContent[i].style.display='none';
    }
};
tabsTitle.click((evt)=>{
    $(tabsTitle).hideAllTabs();
    evt.target.classList.add('active');
    for (let i = 0; i<tabsContent.length; i++){
        if(evt.target.dataset.title === tabsContent[i].dataset.content){
            tabsContent[i].style.display='flex';
        }
    }
});
