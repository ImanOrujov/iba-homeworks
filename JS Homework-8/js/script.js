window.addEventListener("DOMContentLoaded", () => {
    let input = document.createElement("input");
    document.querySelector("script").before(input);
    input.addEventListener("focus", () => {
        input.style.backgroundColor = "green";
        input.style.color = "white";
        input.style.outline = "none";
    });
    document.body.style.display='flex';
    let price = document.createElement('div');
    let close = document.createElement('button');
    close.innerText='x';
    let warning = document.createElement("div");
    input.before(price);
    input.before(close);
    input.after(warning);
    price.style.display='none';
    close.style.display='none';
    warning.style.display='none';
    warning.innerText = "Please enter correct price";
    input.before(price);
    input.before(close);
    input.addEventListener("blur", () => {
        if(typeof parseInt(input.value) == "number" && input.value >= 0){
            input.style.backgroundColor = "white";
            input.style.color = "green";
            price.innerText = `Current price: ${input.value}`;
            price.style.display='block';
            close.style.display='block';
            warning.style.display='none';
            price.style.marginRight = "5px";
            close.style.marginRight = "5px";
            close.addEventListener("click", () => {
                close.style.display = 'none';
                price.style.display = 'none';
                input.value = "";
            });
        }
        else {
            input.style.backgroundColor = "red";
            warning.style.display='block';
            price.style.display='none';
            close.style.display='none';
        }

    })

});

















//         let button = document.createElement("stopButton");
//         button.innerText = "x";
//         let span = document.createElement("span");
//         if (typeof parseInt(input.value) == "number" && input.value >= 0) {
//             input.style.backgroundColor = "white";
//             input.style.color = "green";
//             span.innerText = `Current price: ${input.value}`;
//             input.before(span);
//             input.before(button);
//             span.style.marginRight = "5px";
//             button.style.marginRight = "5px";
//             button.addEventListener("click", () => {
//                 button.style.display='none';
//                 span.style.display='none';
//                 input.value = "";
//             });
//             let warning = document.createElement("div");
//             warning = document.querySelector(".warning");
//             document.body.removeChild(warning);
//         } else {
//             input.style.backgroundColor = "red";
//
//             warning.innerText = "Please enter correct price";
//             warning.classList.add("warning");
//             warning.style.visibility = "visible";
//             input.after(warning);
//         }
//     });
//
//     document.querySelector("script").before(input);
// });